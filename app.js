var express = require('express'),
  //mongoose = require('mongoose'),
  fs = require('fs'),
  config = require('./config/config'),
  passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  mysql = require('mysql');
/*mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db);
});

var modelsPath = __dirname + '/app/models';
fs.readdirSync(modelsPath).forEach(function (file) {
  if (file.indexOf('.js') >= 0) {
    require(modelsPath + '/' + file);
  }
});
*/
var app = express();

require('./config/express')(app, config);
require('./config/routes')(app, mysql);

/* EXEC SHELL COMMANDS
var sys = require('sys')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { sys.puts(stdout) }
exec("ls -la", puts);
*/

app.listen(config.port);

var currentTime = new Date()
var hours = currentTime.getHours()
var minutes = currentTime.getMinutes()
if (minutes < 10){
  minutes = "0" + minutes
}
console.log("listening on " + config.port + " at " + hours +":"+minutes);