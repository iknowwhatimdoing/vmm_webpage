var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'vmmaintainer'
    },
    port: 3000,
    //db: 'mongodb://localhost/vmmaintainer-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'vmmaintainer'
    },
      port: 3000,
   // db: 'mongodb://localhost/vmmaintainer-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'vmmaintainer'
    },
    port: 3000,
   // db: 'mongodb://localhost/vmmaintainer-production'
  }
};

module.exports = config[env];
