define(function(require, exports, module) {

	var app = require("app");
	var Backbone = require("backbone");
	var Editor = require("modules/editor");

	require("bootstrap");

	// Defining the application router.
	var Router = Backbone.Router.extend({
		routes: {
			"": "index",
			"view": "viewVm",
			"create": "createVM",
			"edit": "editVM"
		},

		index: function() {
			this.layout = new Backbone.Layout({
				el: "main",
				template: require("hbs!./templates/main"),
				views: {
					".cons": new Editor.Views.Main()
				}
			}).render().view;
			this.setActive();
		},

		createVM: function() {
			if (sessionStorage.length == 0) {
				this.navigate("#", {trigger: true});
			} else {
				this.layout = new Backbone.Layout({
					el: "main",
					template: require("hbs!./templates/main"),
					views: {
						".cons": new Editor.Views.Creator()
					}
				}).render().view;
				this.setActive();
			}
		},

		editVM: function() {
			if (sessionStorage.length == 0) {
				this.navigate("#", {trigger: true});
			} else {
				this.layout = new Backbone.Layout({
					el: "main",
					template: require("hbs!./templates/main"),
					views: {
						".cons": new Editor.Views.Maintainer()
					}
				}).render().view;
				this.setActive();
			}
		},

		viewVm: function(action) {
			if (sessionStorage.length == 0) {
				this.navigate("#", {trigger: true});
			} else {
				this.layout = new Backbone.Layout({
					el: "main",
					template: require("hbs!./templates/main"),
					views: {
						".cons": new Editor.Views.Display()
					}
				}).render().view;
				this.setActive();
			}
		},

		deleteVM: function() {
			if (sessionStorage.length == 0) {
				this.navigate("#", {trigger: true});
			} else {
				this.layout = new Backbone.Layout({
					el: "main",
					template: require("hbs!./templates/main"),
					views: {
						".cons": new Editor.Views.Delete()
					}
				}).render().view;
				this.setActive();
			}
		},

		setActive: function() {
			this.fragment = Backbone.history.fragment;
			console.log(this.fragment);
			$.each($(".navbar-nav").find("a"), function(k, v){
				$(v).parent().removeClass("active")
				if(this.fragment == $(v).attr("href").substring(1)) {
					$(v).parent().addClass("active");
				}
			}.bind(this));
		},

		initialize: function() {
			$(".logout").on( "click", function() {
				sessionStorage.clear();
				this.navigate("#", {trigger: true});
			}.bind(this));	
			this.bind( "all", function(){
				if (sessionStorage.length == 0) {
					$(".logout").hide();
					if($(".loginForm").length){
						$(".loginForm").show();
					}
				} else {
					$(".logout").show();
					if($(".loginForm").length){
						$(".loginForm").hide();
					}
				}
			});
		}

	});

	module.exports = Router;
});