define(function(require, exports, module) {
	"use strict";

	var app = require("app");

	var VMCollection = Backbone.Collection.extend({
		url: "/api/vms"
	});

	var modelLayout = Backbone.Layout.extend({
		template: require("hbs!./model"),

		serialize: function() {
			return this.model.toJSON();
		},

		render: function(){
			var attributes = this.model.toJSON();
			this.$el.append(this.template(attributes));
		}
	});

	var Layout = Backbone.Layout.extend({
		template: require("hbs!./display"),

		events: {
			'click .vm' : 'loadVM'
		},

		loadVM: function(evt) {
			$.each($("ul.vm-list").find("li"), function(k, v){
				$(v).removeClass("active");
				$(evt.currentTarget).addClass("active");
			}.bind(this));
			//Find the individual model in the collection that was clicked and pass it to the model's own view.
			var singleModel = this.collection.where({VM_ID: $(evt.currentTarget).data("id")})[0];
			var vmView = new modelLayout({
				model: singleModel
			});
			this.setView(".vmArea", vmView).render();
		},

		initialize: function() {
			//make a new collection, listen for a sync, and fetch the data from the server.
			this.collection = new VMCollection();
			this.listenTo(this.collection, "sync", this.render);
			this.collection.fetch({
				data: {
					userId: sessionStorage.getItem("userID"),
					priv: sessionStorage.getItem("priv")
				}
			});
		},

		serialize: function(){
			return this.collection.toJSON();
		}
	});

	module.exports = Layout;
});