define(function(require, exports, module) {
	"use strict";

	var app = require("app");

	var Widget = Backbone.Model.extend({
		url: "/api/vms"
	});

	var Layout = Backbone.Layout.extend({
		template: require("hbs!./creator"),

		events: {
			'click .submitVM': 'saveVM'
		},

		saveVM: function(evt) {
			evt.preventDefault();
			var userId = sessionStorage.getItem("userID"),
				isoName,
				userVals,
				guestOS,
				VM_NAME = this.$el.find(".vmName").val(),
				OS_TYPE = this.$el.find(".OS").val(),
				HDD = this.$el.find(".HDDsize").val(),
				CONFIG_DATA = this.$el.find(".vmConfig").val(),
				diskName = VM_NAME.replace(/\s+/g, '').replace(/[^\w\s]|_/g, "");

			if (VM_NAME == "" || OS_TYPE == "" || HDD == "" || CONFIG_DATA == "") {
				alert("Something was left blank!");
			} else {
				if(OS_TYPE == 1) {
					guestOS = "winxppro";
				} else if (OS_TYPE == 2) {
					guestOS = "windows7-64";
				} else if (OS_TYPE == 3) {
					guestOS = "windows7srv-64";
				} else if(OS_TYPE == 4) {
					isoName = "ubuntu-12.04.4-desktop-amd64";
					guestOS = "linux";
				} else if(OS_TYPE == 5) {
					isoName = "ubuntu-13.10-desktop-amd64";
					guestOS = "linux";
				} else if(OS_TYPE == 6) {
					isoName = "CentOS-6.5-x86_64-LiveCD";
					guestOS = "freebsd"
				}

				if(OS_TYPE == 1 || OS_TYPE == 2 || OS_TYPE == 3){ 
					//Windows uses PXEboot, and doesn't need an iso specified. 
					//User selects what Windows they want when they start up the machine.
					userVals = '\nscsi0:0.fileName = "/vmfs/volumes/datastore1/'+diskName+'/'+diskName+'.vmdk"';
					userVals += '\ndisplayName = "'+VM_NAME+'"';
					userVals += '\nguestOS = "'+guestOS+'"';	
				} else {
					//Otherwise, append the correct iso name.
					userVals = '\nide1:0.fileName = "/vmfs/volumes/datastore1/os_files/'+isoName+'.iso"';
					userVals += '\nscsi0:0.fileName = "/vmfs/volumes/datastore1/'+diskName+'/'+diskName+'.vmdk"';
					userVals += '\ndisplayName = "'+VM_NAME+'"';
					userVals += '\nguestOS = "'+guestOS+'"';				
				}

				CONFIG_DATA += userVals;
				this.model.set({
					"userId": userId,
					"VM_NAME": VM_NAME,
					"OS_TYPE": OS_TYPE,
					"CONFIG_DATA": CONFIG_DATA,
					"HDD": HDD
				});

				this.model.save({
					success: function(model, response) {
						console.log(model);
						console.log(response);
						console.log('success');
						alert(this.model.get("Status"));
					},
					error: function(model, response) {
						console.log(model);
					}
				});
			}
		},

		initialize: function(options) {
			this.model = new Widget();
			this.listenTo(this.model, "change", this.render);
		}

	});

	module.exports = Layout;
});