define(function(require, exports, module) {
	"use strict";

	var app = require("app");

	var loginModel = Backbone.Model.extend({
		url: "/api/users"
	});

	var Layout = Backbone.Layout.extend({
		template: require("hbs!./main"),

		events: {
			'click .subLog': 'processLogin'
		},

		processLogin: function(evt){
			evt.preventDefault();
			var username = this.$el.find(".logun").val(),
				password = this.$el.find(".logpassword").val();

			var loginObj = {
				USERNAME: username,
				PASSWORD: password
			};
			this.model.clear({silent: true});
			this.model.save(loginObj, {
				success: function(data) {
					console.log(data);
					if(!data.attributes.message) {
						this.$el.find(".loginForm").hide();
						sessionStorage.setItem("userID", data.attributes.USER_ID);
						sessionStorage.setItem("priv", data.attributes.PRIV);
						sessionStorage.setItem("token", data.attributes.TOKEN);
					} else {
						alert(data.attributes.message);
					}
				}.bind(this)
			});
		},
 
		initialize: function() {
			this.model = new loginModel();
			this.listenTo(this.model, "change", this.render);
		}
	});

	module.exports = Layout;
});