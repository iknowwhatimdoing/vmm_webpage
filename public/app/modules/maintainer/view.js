define(function(require, exports, module) {
	"use strict";

	var app = require("app");

	var powerModel = Backbone.Model.extend({
		idAttribute: "VM_ID",
		url: "/api/power"
	});

	var VMmodel = Backbone.Model.extend({
		//The data coming back from the server has no attribute named id.
		//Backbone, by default, considers a model's id to be the id value
		//returned from the server. This tells Backbone to instead use 
		//VM_ID as the model's id.
		idAttribute: "VM_ID"
	});

	var VMCollection = Backbone.Collection.extend({
		//Then, tell the collection to use the previously defined model
		//including it's idAttribute. That way we can edit/delete a model because
		//it'll have an individual id.
		model: VMmodel,
		url: "/api/vms"
	});

	var modelLayout = Backbone.Layout.extend({
		template: require("hbs!./model"),

		events: {
			'click .editVM': 'editVM',
			'click .deleteVM': 'deleteVM',
			'click .powerVM': 'powerVM',
			'click .suspendVM': 'suspendVM',
			'click .resetVM': 'resetVM'
		},

		editVM: function(evt) {
			evt.preventDefault();
			var REASON = this.$el.find(".editReason").val(),
				CONFIG_DATA = this.$el.find(".VMconfig").val();

			if (REASON == "" || CONFIG_DATA == "") {
				alert("Something was left blank!");
			} else {
				this.model.set({
					"userId": sessionStorage.getItem("userID"),
					"REASON": REASON,
					"CONFIG_DATA": CONFIG_DATA
				});
				this.model.save();
			}
		},

		deleteVM: function() {
			var confirmation = confirm("Are you sure you want to delete this virtual machine?");
			if (confirmation==true)	{
				this.model.destroy({
					headers: { 
						userId: sessionStorage.getItem("userID"),
						VM_NAME: this.model.get("VM_NAME").replace(/\s+/g, '')
					},
					success: function(model, response) {
						console.log("DELETED!");
					}
				});
			} else {
				return false
			}
		},

		powerVM: function() {
			if(this.powerModel.get("state") == 0){
				this.powerModel.set("state", 1);
			} else {
				this.powerModel.set("state", 0);
			}
			this.powerModel.save({
				vmID: this.model.get("VM_ID")},
				{
					success: function(data) {
						console.log(data);
						this.model.set("state", data.attributes.state);
					}.bind(this)
				}
			);
		},

		suspendVM: function() {
			this.powerModel.set("state", 3);
			this.powerModel.save({
				vmID: this.model.get("VM_ID")},
				{
					success: function(data) {
						console.log(data);
						this.model.set("state", data.attributes.state);
					}.bind(this)
				}
			);			
		},

		resetVM: function() {
			this.powerModel.set("state", 2);
			this.powerModel.save({
				vmID: this.model.get("VM_ID")},
				{
					success: function(data) {
						console.log(data);
						this.model.set("state", data.attributes.state);
					}.bind(this)
				}
			);	
		},

		initialize: function(){
			//listen to a change on the "state" attribute. 
			//It comes with a slight delay, so rerender when it does arrive.
			this.listenTo(this.model, "change:state", this.render);

			//create a new power model... blah blah, same as below.
			this.powerModel = new powerModel();
			this.powerModel.fetch({
				data: {
					vmID: this.model.get("VM_ID")
				},
				success: function(data) {
					this.model.set("state", data.attributes.state);
				}.bind(this)
			});
		},

		serialize: function() {
			return this.model.toJSON();
		},

		render: function() {
			var attributes = this.model.toJSON();
			this.$el.append(this.template(attributes));
		},

		afterRender: function() {
			//if off
			if(this.model.get("state") == 0) {
				$(".VMconfig").attr("disabled", false);
				$(".editVM").removeClass("disabled");
				$(".deleteVM").removeClass("disabled");
				$(".suspendVM").addClass("disabled");
				$(".resetVM").addClass("disabled");
			} else if(this.model.get("state") == 1) { //if on
				$(".VMconfig").attr("disabled", true);
				$(".editVM").addClass("disabled");
				$(".deleteVM").addClass("disabled");
				$(".suspendVM").removeClass("disabled");
				$(".resetVM").removeClass("disabled");
			} else if (this.model.get("state") == 3) { //suspended
				$(".VMconfig").attr("disabled", true);
				$(".editVM").addClass("disabled");
				$(".deleteVM").removeClass("disabled");
				$(".resetVM").addClass("disabled");
			}
		}
	});

	var Layout = Backbone.Layout.extend({
		template: require("hbs!./maintainer"),

		events: {
			'click .vm' : 'loadVM',
			'click .submitConfig' : 'saveConfig'
		},

		loadVM: function(evt) {
			$.each($("ul.vm-list").find("li"), function(k, v){
				$(v).removeClass("active");
				$(evt.currentTarget).addClass("active");
			}.bind(this));
			//Find the individual model in the collection that was clicked and pass it to the model's own view.
			var singleModel = this.collection.where({VM_ID: $(evt.currentTarget).data("id")})[0];
			console.log("Layout", singleModel);

			var vmView = new modelLayout({
				model: singleModel
			});
			this.setView(".vmArea", vmView).render();
		},

		initialize: function() {
			//make a new collection, listen for a sync, and fetch the data from the server.
			this.collection = new VMCollection();
			this.listenTo(this.collection, "sync", this.render);
			this.collection.fetch({
				data: {
					userId: sessionStorage.getItem("userID"),
					priv: sessionStorage.getItem("priv")
				}
			});

		},

		serialize: function() {
			return this.collection.toJSON();
		}
	});

	module.exports = Layout;
});