define(function(require, exports, module) {

	module.exports = {
		Views: {
			Main: require("./main/view"), //Main page
			Creator: require("./creator/view"), //Create VM page
			Display: require("./display/view"), //View VM page
			Maintainer: require("./maintainer/view") //Edit/Delete VM page
		}
	};

});