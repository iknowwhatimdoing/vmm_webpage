define(function(require, exports, module) {

	// External dependencies.
	var _ = require("underscore");
	var $ = require("jquery");
	var Backbone = require("backbone");
	var Layout = require("layoutmanager");
	var handlebars = require("handlebars");

	// Alias the module for easier identification.
	var app = module.exports;

	// The root path to run the application through.
	app.root = "/";

	Handlebars.registerHelper("debug", function (optionalValue) {
		console.log("Current Context", this);

		if (optionalValue) {
			console.log("Value", optionalValue);
		}
	});

	Handlebars.registerHelper('ifvalue', function (options) {
	  if (options.hash.value == this.state) {
	    return options.fn(this)
	  } else {
	    return options.inverse(this);
	  }
	});

});